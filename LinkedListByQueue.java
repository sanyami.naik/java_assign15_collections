package Assignments.Fifteen;

import java.util.LinkedList;
import java.util.Queue;

public class LinkedListByQueue {

    public static void main(String[] args) {
        Queue queue=new LinkedList();
        queue.add(23);
        queue.add(78);
        queue.add(230);
        queue.add(243);


        System.out.println("the peeked object is "+queue.peek());
        for (Object o:queue) {
            System.out.println(o);
        }


    }
}



/*OUTPUT
the peeked object is 23
23
78
230
243
 */