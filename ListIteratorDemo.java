package Assignments.Fifteen;

import java.util.ArrayList;
import java.util.ListIterator;

class Student{
    int id;
    String name;

    public Student(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
public class ListIteratorDemo {
    public static void main(String[] args) {
        ArrayList<Student> arrayList=new ArrayList();
        arrayList.add(new Student(10,"Sanyami"));
        arrayList.add(new Student(20,"Praveer"));
        arrayList.add(new Student(30,"Ashraf"));

        System.out.println(arrayList);                   //Prints the alist of adres of the elements
        ListIterator listIterator=arrayList.listIterator();
        while(listIterator.hasNext())
        {
            System.out.println(listIterator.nextIndex());
            System.out.println(listIterator.next());
        }

        while(listIterator.hasPrevious())
        {
            System.out.println(listIterator.previous());
            System.out.println(listIterator.previousIndex());
        }






    }

}


/*OUTPUT
[Student{id=10, name='Sanyami'}, Student{id=20, name='Praveer'}, Student{id=30, name='Ashraf'}]
0
Student{id=10, name='Sanyami'}
1
Student{id=20, name='Praveer'}
2
Student{id=30, name='Ashraf'}
Student{id=30, name='Ashraf'}
1
Student{id=20, name='Praveer'}
0
Student{id=10, name='Sanyami'}
-1
 */