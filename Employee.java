package Assignments.Fifteen;

import java.util.*;


class SortBySalary implements Comparator<Employee> {


    public int compare(Employee employee1,Employee employee2) {
        return (int)(employee1.salary-employee2.salary);
    }
}

class SortByName implements Comparator<Employee> {
    @Override
    public int compare(Employee employee1, Employee employee2) {
        return employee1.name.compareTo(employee2.name);
    }
}

public class
Employee {

    int id;
    String name;
    float salary;

    public Employee(int id, String name, float salary) {
        this.id = id;
        this.name = name;
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", salary=" + salary +
                '}';
    }

    public static void main(String[] args) {

        ArrayList<Employee> arrayList=new ArrayList();
        arrayList.add(new Employee(1,"Sanyami",87520.0f));
        arrayList.add(new Employee(5,"Ashraf",5000.0f));
        arrayList.add(new Employee(10,"Praveer",852.0f));
        arrayList.add(new Employee(3,"Aman",-10000.0f));
        arrayList.add(new Employee(198,"Ayushman",150.0f));


        for (Employee e:arrayList) {
            System.out.println(e);
        }


        System.out.println("sorting by name");
        Collections.sort(arrayList,new SortByName());
        for (Employee e:arrayList) {
            System.out.println(e);
        }

        System.out.println("sorting by salary");
        Collections.sort(arrayList,new SortBySalary());
        for (Employee e:arrayList) {
            System.out.println(e);
        }

        System.out.println("employees with salary greater than 15000");
        arrayList.stream().filter(p->p.salary>15000).forEach(System.out::println);
    }

}


/*OUTPUT:
Employee{id=1, name='Sanyami', salary=87520.0}
Employee{id=5, name='Ashraf', salary=5000.0}
Employee{id=10, name='Praveer', salary=852.0}
Employee{id=3, name='Aman', salary=-10000.0}
Employee{id=198, name='Ayushman', salary=150.0}
sorting by name
Employee{id=3, name='Aman', salary=-10000.0}
Employee{id=5, name='Ashraf', salary=5000.0}
Employee{id=198, name='Ayushman', salary=150.0}
Employee{id=10, name='Praveer', salary=852.0}
Employee{id=1, name='Sanyami', salary=87520.0}
sorting by salary
Employee{id=3, name='Aman', salary=-10000.0}
Employee{id=198, name='Ayushman', salary=150.0}
Employee{id=10, name='Praveer', salary=852.0}
Employee{id=5, name='Ashraf', salary=5000.0}
Employee{id=1, name='Sanyami', salary=87520.0}
employees with salary greater than 15000
Employee{id=1, name='Sanyami', salary=87520.0}

 */