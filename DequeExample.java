package Assignments.Fifteen;


import java.util.*;

public class DequeExample {
    public static void main(String[] args)
    {
        Deque<Integer> deque = new LinkedList<>();


        // Add at the last
        deque.add(80);
        deque.add(80);
        deque.add(80);

        // Add at the first
        deque.offerFirst(100);

        // Add at the first
        deque.addFirst(30);

        // Add at the first
        deque.push(50);



        // Add at the last
        deque.addLast(40);

        // Add at the last
        deque.offer(60);

        System.out.println(deque + "\n");
        deque.poll();                      //bydefault pops out the first element(pollFirst() and pollLast())
        System.out.println(deque.peek());
        System.out.println(deque);



        // We can remove the first element
        // or the last element.
        deque.removeFirst();
        deque.removeLast();
        System.out.println("Deque after removing "+ "first and last: "+ deque);
    }
}




/*
OUTPUT
[50, 30, 100, 80, 80, 80, 40, 60]

30
[30, 100, 80, 80, 80, 40, 60]
Deque after removing first and last: [100, 80, 80, 80, 40]
 */